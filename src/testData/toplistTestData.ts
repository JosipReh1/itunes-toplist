export const feedResponse = {
    results: [
        {
            "artistName":"Sam Hunt",
            "id":"1481997471",
            "releaseDate":"2019-10-11",
            "name":"Kinfolks",
            "collectionName":"Kinfolks - Single",
            "kind":"song",
            "copyright":"An MCA Nashville Release; ℗ 2019 UMG Recordings, Inc.",
            "artistId":"214150835",
            "artistUrl":"https://music.apple.com/us/artist/sam-hunt/214150835?app=itunes",
            "artworkUrl100":"https://is4-ssl.mzstatic.com/image/thumb/Music113/v4/73/8a/37/738a3749-0a1b-598c-9650-a035e157cdf5/19UMGIM84273.rgb.jpg/200x200bb.png",
            "genres":[{"genreId":"6","name":"Country","url":"https://itunes.apple.com/us/genre/id6"},{"genreId":"34","name":"Music","url":"https://itunes.apple.com/us/genre/id34"}],
            "url":"https://music.apple.com/us/album/kinfolks/1481997451?i=1481997471&app=itunes"
        }
    ]
};

export const countriesData = [
    {label: "United States", value: "us", selected: true},
    {label: "United Kingdom", value: "gb", selected: false},
     {label: "Germany", value: "de", selected: false}
];

export const trackTypesData = [
    {label: "Hot tracks", value: "hot-tracks", selected: true},
    {label: "New music", value: "new-music", selected: false},
    {label: "Recent releases", value: "recent-releases", selected: false},
    {label: "Top albums", value: "top-albums", selected: false},
    {label: "Top songs", value: "top-songs", selected: false}
];
