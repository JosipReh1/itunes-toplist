import {GenreDto} from "./genre.dto";

export interface TrackDto {
    artistId: string;
    artistName: string;
    artistUrl: string;
    artworkUrl100: string;
    collectionName: string;
    copyright: string;
    genres: GenreDto[];
    id: string;
    kind: string;
    name: string;
    releaseDate: string;
    url: string;
}
