import { curry } from 'ramda';

export const startsWith = curry((q, str) => str.startsWith(q));

export const endsWith = curry((q, str) => str.endsWith(q));
