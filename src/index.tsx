import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import * as serviceWorker from './serviceWorker';
import {ConnectedRouter} from "connected-react-router";
import Routes from "./Routes";
import { Provider } from 'react-redux';
import { configureStore, history, runSaga } from './store/configureStore/configureStore';
import './styles/index.scss';

const store = configureStore();
runSaga();
Modal.setAppElement('#root');
ReactDOM.render(
    <div>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Routes />
            </ConnectedRouter>
        </Provider></div>,
    document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
