import axios, {AxiosRequestConfig, Method} from 'axios';
import { CANCEL } from 'redux-saga';
import { assoc } from 'ramda';

const appleAPI = axios.create({
    baseURL: "https://cors-anywhere.herokuapp.com/https://rss.itunes.apple.com/api/v1/",
});
export { appleAPI };

const createRequestObject = (method: Method, url: string, data: object): AxiosRequestConfig => ({
    data,
    method,
    url
});

export const cancelableHttpRequest = (method: Method, url: string, config: AxiosRequestConfig) => {
    const source = axios.CancelToken.source();

    const requestObject = createRequestObject(method, url, config);
    const cancellableRequestObject = assoc('cancelToken', source.token)(
        requestObject
    );

    const request = appleAPI.request(cancellableRequestObject);
    //@ts-ignore
    request[CANCEL] = () => source.cancel();
    return request;
};
