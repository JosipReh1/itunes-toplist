import {action} from "typesafe-actions";
import {ToplistActionTypes} from "./types";

export const mergeState = (obj: object) => action(ToplistActionTypes.MERGE_STATE, obj);
export const fetchTracks = (country: string, trackType: string) => action(ToplistActionTypes.FETCH_TOPLIST, {country, trackType});
export const fetchToplistFailure = () => action(ToplistActionTypes.FETCH_TOPLIST_FAILURE);
export const fetchToplistSuccess = () => action(ToplistActionTypes.FETCH_TOPLIST_SUCCESS);
export const searchTracks = (searchTerm: string) => action(ToplistActionTypes.SEARCH, searchTerm);
export const selectCountry = (country: string) => action(ToplistActionTypes.SELECT_COUNTRY, country);
export const selectFeed = (feed: string) => action(ToplistActionTypes.SELECT_FEED, feed);
