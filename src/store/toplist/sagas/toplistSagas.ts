import {all, put, call, takeLatest, select, takeEvery} from "redux-saga/effects";
import {ToplistActionTypes, ToplistState} from "../types";
import { toLower, filter, equals } from 'ramda';
import { startsWith } from '../../../utils/ramdaUtils';
import {
    fetchToplistFailure,
    fetchToplistSuccess,
    fetchTracks,
    mergeState,
    searchTracks,
    selectCountry, selectFeed
} from "../actions";
import {fetchTracksAPI} from "../api";
import {selectToplist} from "../selectors";
import {TrackDto} from "../../../dto/track.dto";

export function* fetchTracksSaga({ payload: {country, trackType}}: ReturnType<typeof fetchTracks>) {
    try {
        yield call(updateSearchValue, '');
        if (country && trackType) {
            const { data: {feed} } = yield call(fetchTracksAPI, country, trackType);
            yield put(mergeState({
                allData: feed.results,
                filteredData: feed.results,
            }));
            yield put(fetchToplistSuccess());
        }
    } catch (e) {
        yield put(fetchToplistFailure());
    }
}

export function* updateSearchValue(searchTerm: string) {
    yield put(mergeState({form : {searchTerm}}));
}

export function* searchTracksSaga({ payload: searchTerm }: ReturnType<typeof searchTracks>) {
    yield call(updateSearchValue, searchTerm);
    const { allData } : ToplistState = yield select(selectToplist);

    const filterByArtistName = filter((item: TrackDto) => (
        startsWith(toLower(searchTerm))(toLower(item.artistName))
    ))(allData);

    const filterBySongname = filter((item: TrackDto) => (
        startsWith(toLower(searchTerm))(toLower(item.name))
    ))(allData);

    const results = [...filterByArtistName, ...filterBySongname];
    yield put(mergeState({
        filteredData: searchTerm.length > 0 ? results : allData
    }));
}

export function* selectCountrySaga({payload: selectedCountry}: ReturnType<typeof selectCountry>) {
    const { form: {countries} } : ToplistState = yield select(selectToplist);
    yield put(mergeState({
        form: {
            countries: countries.map((country) => ({
                ...country,
                selected: equals(country.value, selectedCountry)
            }))
        }
    }))
}

export function* selectFeedTypeSaga({payload: feedType}: ReturnType<typeof selectFeed>) {
    const { form: {trackTypes} } : ToplistState = yield select(selectToplist);
    yield put(mergeState({
        form: {
            trackTypes: trackTypes.map((type) => ({
                ...type,
                selected: equals(type.value, feedType)
            }))
        }
    }))
}

export function* toplistSagas() {
    yield all([
        takeLatest(ToplistActionTypes.FETCH_TOPLIST, fetchTracksSaga),
        takeLatest(ToplistActionTypes.SEARCH, searchTracksSaga),
        takeEvery(ToplistActionTypes.SELECT_COUNTRY, selectCountrySaga),
        takeEvery(ToplistActionTypes.SELECT_FEED, selectFeedTypeSaga),
    ])
}
