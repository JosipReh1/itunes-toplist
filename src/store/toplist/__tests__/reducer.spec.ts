import {initialState, toplistReducer} from "../reducer";
import {fetchToplistFailure, fetchToplistSuccess, fetchTracks, mergeState} from "../actions";

describe('toplistReducer', () => {
    it('should return initial state', () => {
        expect(toplistReducer(initialState, {type:'', payload: ''})).toEqual({
            ...initialState
        });
    });

    it('should update state on fetch action', () => {
        expect(toplistReducer(initialState, fetchTracks('us', 'new-music'))).toEqual({
            ...initialState,
            inProgress: true,
            error: false
        });
    });

    it('should update error field in state while fetch fails', () => {
        expect(toplistReducer(initialState, fetchToplistFailure())).toEqual({
            ...initialState,
            inProgress: false,
            error: true,
        });
    });

    it('should update inProgress and error fields when fetch completes', () => {
        expect(toplistReducer(initialState, fetchToplistSuccess())).toEqual({
            ...initialState,
            inProgress: false,
            error: false
        });
    });

    it('should merge anything to state', () => {
        expect(toplistReducer(initialState, mergeState({newKey: 'newValue'}))).toEqual({
            ...initialState,
            newKey: 'newValue'
        });
    });

    it('should update existing field in state', () => {
        expect(toplistReducer(initialState, mergeState({inProgress: true}))).toEqual({
            ...initialState,
            inProgress: true
        })
    });
});
