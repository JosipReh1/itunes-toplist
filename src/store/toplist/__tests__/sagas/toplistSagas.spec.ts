import { testSaga } from "redux-saga-test-plan";
import {takeEvery, takeLatest} from 'redux-saga/effects';
import {
    fetchTracksSaga,
    searchTracksSaga,
    selectCountrySaga,
    selectFeedTypeSaga,
    toplistSagas, updateSearchValue
} from "../../sagas/toplistSagas";
import {ToplistActionTypes} from "../../types";
import {
    fetchToplistFailure,
    fetchToplistSuccess,
    fetchTracks,
    mergeState,
    searchTracks,
    selectCountry, selectFeed
} from "../../actions";
import {fetchTracksAPI} from "../../api";
import {countriesData, feedResponse, trackTypesData} from "../../../../testData/toplistTestData";
import {selectToplist} from "../../selectors";

describe('toplist sagas', () => {
    describe('root toplist sagas', () => {
        it('should create root saga', () => {
            const saga = testSaga(toplistSagas);
            saga.next().all([
                takeLatest(ToplistActionTypes.FETCH_TOPLIST, fetchTracksSaga),
                takeLatest(ToplistActionTypes.SEARCH, searchTracksSaga),
                takeEvery(ToplistActionTypes.SELECT_COUNTRY, selectCountrySaga),
                takeEvery(ToplistActionTypes.SELECT_FEED, selectFeedTypeSaga),
            ]);
            saga.next().isDone();
        });
    });

    describe('should fetch tracks', () => {
        const country = 'us';
        const trackType = 'top-albums';
        const action = fetchTracks(country, trackType);

        it('should successfully fetch tracks', () => {
            const saga = testSaga(fetchTracksSaga, action);
            saga.next().call(updateSearchValue, '');
            saga.next().call(fetchTracksAPI, country, trackType);
            saga.next({ data: {feed: feedResponse} }).put(mergeState({
                allData: feedResponse.results,
                filteredData: feedResponse.results,
            }));
            saga.next().put(fetchToplistSuccess());
            saga.next().isDone();
        });

        it('should dispatch error action if fetch fails', () => {
            const saga = testSaga(fetchTracksSaga, action);
            saga.next().throw(new Error('')).put(fetchToplistFailure());
            saga.next().isDone();
        });
    });
        describe('updateSearchValue', () => {
            it('should merge new search value to store', () => {
                const searchTerm = 'search';
                const saga = testSaga(updateSearchValue, searchTerm);
                saga.next().put(mergeState({form : {searchTerm}}));
                saga.next().isDone();
            });
        })

    describe('searchTracksSaga', () => {
        it('should filter tracks and return one result', () => {
            const searchTerm = 'Sa';
            const allData = feedResponse.results;
            const action = searchTracks(searchTerm);
            const saga = testSaga(searchTracksSaga, action);
            saga.next().call(updateSearchValue, searchTerm);
            saga.next().select(selectToplist);

            saga.next({allData}).put(mergeState({
                filteredData: allData
            }));
        });
        it('should filter tracks by unknown search criteria and return no results', () => {
            const searchTerm = 'xe';
            const allData = feedResponse.results;
            const action = searchTracks(searchTerm);
            const saga = testSaga(searchTracksSaga, action);
            saga.next().call(updateSearchValue, searchTerm);
            saga.next().select(selectToplist);

            saga.next({allData}).put(mergeState({
                filteredData: []
            }));
        });
    });
    describe('selectCountrySaga',  () => {
        it('should select country by provided parameter', () => {
            const action = selectCountry('gb');
            const saga = testSaga(selectCountrySaga, action);
            const expectedCountries = [
                {label: "United States", value: "us", selected: false},
                {label: "United Kingdom", value: "gb", selected: true},
                {label: "Germany", value: "de", selected: false}
            ];
            saga.next().select(selectToplist);
            saga.next({form: {countries: countriesData}}).put(mergeState({
                form: {
                    countries: expectedCountries
                }
            }));
            saga.next().isDone();
        })
    });
    describe('selectFeedTypeSaga', () => {
        it('should select feed type', () => {
            const action = selectFeed('new-music');
            const saga = testSaga(selectFeedTypeSaga, action);
            const expectedTrackTypes = [
                {label: "Hot tracks", value: "hot-tracks", selected: false},
                {label: "New music", value: "new-music", selected: true},
                {label: "Recent releases", value: "recent-releases", selected: false},
                {label: "Top albums", value: "top-albums", selected: false},
                {label: "Top songs", value: "top-songs", selected: false}
            ];
            saga.next().select(selectToplist);
            saga.next({form: {trackTypes: trackTypesData}}).put(mergeState({
                form: {
                    trackTypes: expectedTrackTypes
                }
            }));
            saga.next().isDone();
        });
    })
});
