import {cancelableHttpRequest} from "../../api";

export const fetchTracksAPI = (country: string, trackType: string) =>
    cancelableHttpRequest('get', `${country}/itunes-music/${trackType}/all/100/explicit.json`, {});
