import {TrackDto} from "../../dto/track.dto";
import {MenuItem} from "../../components/Dropdown/types";

export enum ToplistActionTypes {
    FETCH_TOPLIST = '@@TOPLIST/FETCH_TOPLIST',
    FETCH_TOPLIST_SUCCESS = '@@TEST/FETCH_TOPLIST_SUCCESS',
    FETCH_TOPLIST_FAILURE = '@@TEST/FETCH_TOPLIST_FAILURE',
    MERGE_STATE = '@@Toplist/MERGE_STATE',
    SEARCH = '@@Toplist/SEARCH',
    SELECT_COUNTRY = '@@Toplist/SELECT_COUNTRY',
    SELECT_FEED = '@@Toplist/SELECT_FEED'
};

export interface formConfig {
    [key:string]: {
        label: string;
        apiKeyName: string;
    }
}

export interface ToplistState {
    inProgress: boolean;
    error: boolean;
    allData: TrackDto[];
    filteredData: TrackDto[];
    availableCountries: formConfig;
    feedTypes: formConfig;
    form: {
        countries: MenuItem[],
        trackTypes: MenuItem[],
        searchTerm: ''
    }
};
