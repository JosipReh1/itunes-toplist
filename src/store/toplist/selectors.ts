import {ApplicationState} from "../configureStore/reducers";

export const selectToplist = (store: ApplicationState) => store.toplist;
