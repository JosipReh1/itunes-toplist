import {AnyAction, Reducer} from 'redux';
import {formConfig, ToplistActionTypes, ToplistState} from "./types";
import {keys, mergeDeepRight} from "ramda";


export const availableCountries: formConfig = {
    US: {
        label: 'United States',
        apiKeyName: 'us'
    },
    GB: {
        label: 'United Kingdom',
        apiKeyName: 'gb'
    },
    DE: {
        label: 'Germany',
        apiKeyName: 'de'
    }
};

export const feedTypes: formConfig = {
    HOT_TRACKS: {
        label: 'Hot tracks',
        apiKeyName: 'hot-tracks',
    },
    NEW_MUSIC: {
        label: 'New music',
        apiKeyName: 'new-music'
    },
    RECENT_RELEASES: {
        label: 'Recent releases',
        apiKeyName: 'recent-releases'
    },
    TOP_ALBUMS: {
        label: 'Top albums',
        apiKeyName: 'top-albums'
    },
    TOP_SONGS: {
        label: 'Top songs',
        apiKeyName: 'top-songs'
    }
};

export const initialState: ToplistState = {
    inProgress: false,
    error: false,
    allData: [],
    filteredData: [],
    availableCountries,
    feedTypes,
    form: {
        countries: keys(availableCountries).map((country, index) => ({
            label: availableCountries[country].label,
            value: availableCountries[country].apiKeyName,
            selected: index === 0
        })),
        trackTypes: keys(feedTypes).map((type, index) => ({
            label: feedTypes[type].label,
            value: feedTypes[type].apiKeyName,
            selected: index === 0
        })),
        searchTerm: ''
    }
};

const reducer: Reducer<ToplistState> = (state = initialState, action) => {
    switch (action.type) {
        case ToplistActionTypes.FETCH_TOPLIST: {
            return {
                ...state,
                inProgress: true,
                error: false
            }
        }
        case ToplistActionTypes.FETCH_TOPLIST_FAILURE: {
            return {
                ...state,
                inProgress: false,
                error: true
            }
        }
        case ToplistActionTypes.FETCH_TOPLIST_SUCCESS: {
            return {
                ...state,
                inProgress: false,
                error: false
            }
        }
        case ToplistActionTypes.MERGE_STATE: {
            return mergeDeepRight<ToplistState, AnyAction>(state, action.payload);
        }
        default: {
            return state
        }
    }
};

export { reducer as toplistReducer }
