import {testSaga} from "redux-saga-test-plan";
import {rootSaga} from "../rootSaga";
import {toplistSagas} from "../toplist/sagas/toplistSagas";
import {call, put} from "redux-saga/effects";
import {fetchToplistFailure} from "../toplist/actions";

describe('rootSaga', () => {
    it('should setup root saga', () => {
        const saga = testSaga(rootSaga);
        saga.next().all([
            call(toplistSagas),
        ]);
    });

    it('should dispatch failure action when error is thrown', () => {
        const saga = testSaga(rootSaga);
        saga.next().throw(new Error('')).put(fetchToplistFailure());
    });
});
