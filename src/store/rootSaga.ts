import {all, call, put } from "redux-saga/effects";
import {toplistSagas} from "./toplist/sagas/toplistSagas";
import {fetchToplistFailure} from "./toplist/actions";

export function* rootSaga() {
  while (true) {
    try {
      yield all([
          call(toplistSagas),
      ]);
    } catch (err) {
      yield put(fetchToplistFailure())
    }
  }
}
