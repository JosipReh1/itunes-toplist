import {connectRouter, RouterState} from "connected-react-router";
import { History } from 'history';
import {combineReducers} from "redux";
import {toplistReducer} from "../toplist/reducer";
import {ToplistState} from "../toplist/types";

export interface ApplicationState {
    router: RouterState,
    toplist: ToplistState
}

export default function createRootReducer(history: History) {
    return combineReducers<ApplicationState>({
        router: connectRouter(history),
        toplist: toplistReducer,
    });
}
