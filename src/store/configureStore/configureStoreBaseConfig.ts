import { routerMiddleware } from 'connected-react-router';
import { History, createBrowserHistory } from "history";
import {Middleware} from "redux";
import createSagaMiddleware from 'redux-saga';
import createRootReducer from './reducers';

export const history: History = createBrowserHistory({basename: process.env.PUBLIC_URL});
export const router: Middleware = routerMiddleware(history);
export const rootReducer = createRootReducer(history);
export const sagaMiddleware = createSagaMiddleware();

export const baseMiddleware: Middleware[] = [sagaMiddleware, router];
