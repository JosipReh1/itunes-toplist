import { routerActions } from 'connected-react-router';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { createLogger } from 'redux-logger';
import { rootSaga } from '../rootSaga';
import {
  baseMiddleware,
  history,
  rootReducer,
  sagaMiddleware,
} from './configureStoreBaseConfig';


const configureStore = (initialState = {}) => {
  // Logging Middleware
  const logger = createLogger({
      collapsed: true,
      level: 'info',
  });

  // Skip redux logs in console during the tests
  if (process.env.NODE_ENV !== 'test') {
    baseMiddleware.push(logger);
  }

  // Redux DevTools Configuration
  const actionCreators = {
    ...routerActions,
  };


  const composeEnhancers = composeWithDevTools({
    actionCreators,
    trace: true,
  });

  // Create Store
  const store = createStore(rootReducer, initialState,
    composeEnhancers(
      applyMiddleware(...[...baseMiddleware]),
    ));

  if (module.hot) {
    module.hot.accept(
      './reducers',
      () => store.replaceReducer(require('./reducers').default),
    );
  }
  return store;
};

const runSaga = () => sagaMiddleware.run(rootSaga);

export default { configureStore, history, runSaga };
