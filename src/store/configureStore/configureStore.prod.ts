import {applyMiddleware, compose, createStore, Store} from 'redux';
import { rootSaga } from '../rootSaga';
import {
    baseMiddleware,
    history,
    rootReducer,
    sagaMiddleware
} from './configureStoreBaseConfig';
import {ApplicationState} from "./reducers";

// Apply Middleware & Compose Enhancers
const appliedMiddleware = applyMiddleware(
    ...baseMiddleware
);

const enhancer = compose(appliedMiddleware);

function configureStore(initialState = {}): Store<ApplicationState> {
    return createStore(rootReducer, initialState, enhancer);
}

const runSaga = () => sagaMiddleware.run(rootSaga);

export default { configureStore, history, runSaga };
