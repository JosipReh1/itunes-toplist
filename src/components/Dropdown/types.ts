export interface MenuItem {
    label: string;
    selected: boolean;
    value: string;
}
