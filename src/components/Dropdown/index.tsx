import React, {Dispatch, SetStateAction, useRef, useState} from "react";
import classNames from 'classnames';
import './dropdown.scss';
import {useOutsideAlerter} from "../../hooks/outsideAlertHook";
import {getSelectedItem} from "./util";
import {ArrowDown, ArrowUp, Check} from "../../icons";
import {MenuItem} from "./types";

interface Props {
    data: MenuItem[];
    onChange?: (value: string) => void;
}


const Dropdown: React.FC<Props> = ({data, onChange}) => {
    const [isOpen, setIsOpen] = useState(false);
    const [items, setItems]: [MenuItem[], Dispatch<SetStateAction<MenuItem[]>>] = useState(data);

    const toggleMenu = () => {
        setIsOpen(!isOpen);
    };

    const handleOutsideClick = () => {
        if (isOpen) {
            toggleMenu();
        }
    };

    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef, handleOutsideClick);

    const handleSelect = (selectedLabel: string) => () => {
        const updated = items.map(item => ({
            label: item.label,
            selected: item.value === selectedLabel,
            value: item.value
        }));
        setItems(updated);
        toggleMenu();
        onChange && onChange(selectedLabel);
    };

    return (
        <div ref={wrapperRef} className="dropdown-wrapper">
            <div className="dropdown-header" onClick={toggleMenu}>
                <div className="dropdown-header-title">{getSelectedItem(items)}</div>
                {isOpen ? <ArrowUp /> : <ArrowDown />}
            </div>
            {isOpen && <ul className="dropdown-list" onClick={e => e.stopPropagation()}>
                {items.map((item: MenuItem)=> (
                    <li className={classNames({'selected': item.selected}, 'dropdown-list-item')} key={item.value} onClick={handleSelect(item.value)}>
                        <span>{item.label}</span> {item.selected && <Check/>}
                    </li>
                ))}
            </ul>}
        </div>
    );
};

export default Dropdown;
