import * as R from 'ramda';
import {MenuItem} from "./types";

export const getSelectedItem = (items: MenuItem[], property = 'label') =>
    R.pathOr('', [property], items.find(element => element.selected === true));

