import React from 'react'
import Modal from 'react-modal';
import {TrackDto} from "../../dto/track.dto";
import {Loading} from "../../icons";
import './trackModal.scss';
import {modalStyles} from "./styles";

interface Props {
    isOpen: boolean;
    closeModal: () => void;
    selectedTrack: TrackDto;
}

const TrackModal: React.FC<Props> = ({isOpen, closeModal, selectedTrack}) => {
    const prettifyGenres = () => selectedTrack.genres.map((genre) => genre.name);

    const renderModal = () => <Modal
        isOpen={isOpen}
        onRequestClose={closeModal}
        overlayClassName="Overlay"
        style={modalStyles}
    >
        <div className='modal-close-btn' onClick={closeModal}>X</div>
        <div className='modal-track'>
            <div className='modal-artwork'>
                <img alt={selectedTrack.artistName} src={selectedTrack.artworkUrl100} />
            </div>
            <div className='track-info-container'>
                <span className='track-copyright'>{selectedTrack.copyright}</span>
            </div>
            <div className="track-info-container">
                <span>
                    <span>Artist: </span>
                    <span><a href={selectedTrack.artistUrl} target='_blank' rel="noopener noreferrer">{selectedTrack.artistName}</a></span>
                </span>
                <span>
                    <span>{selectedTrack.kind === 'song' ? 'Song:' : 'Album:'} </span>
                    <span><a href={selectedTrack.url} target='_blank' rel="noopener noreferrer">{selectedTrack.name}</a></span>
                </span>
                <span>
                    <span>Genres:</span> <span>{prettifyGenres().toString()}</span>
                </span>
                <span>
                    <span>Release date:</span> <span>{selectedTrack.releaseDate || 'Unknown'}</span>
                </span>
            </div>
        </div>
    </Modal>;

    const renderLoading = () => <Loading />;

    if (isOpen && selectedTrack) {
        return renderModal();
    } else if(isOpen && !selectedTrack) {
        return renderLoading();
    } else {
        return null;
    }
};
export default TrackModal;
