export const modalStyles = {
    content : {
        top: '50%',
        width: '40%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        padding: '0',
        border: 'none'
    }
};
