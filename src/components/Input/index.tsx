import React from 'react';
import classNames from 'classnames';
import './input.scss';

interface Props extends Omit<React.InputHTMLAttributes<HTMLInputElement>, 'onChange' | 'value'> {
    value: string;
    onChange(value: React.ChangeEvent<HTMLInputElement>): void;
    leftIcon?: React.ReactElement
    rightIcon?: React.ReactElement
}

export const Input: React.FunctionComponent<Props> = ({
    leftIcon,
    rightIcon,
    children,
    onChange,
    ...shared
}) => {
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        onChange(event);
    };

    const clazzNames = classNames({
        'with-left-icon': leftIcon,
        'with-right-icon': rightIcon,
        'with-both-icons' : leftIcon && rightIcon
    }, 'form-input');

    return (
        <div className={clazzNames}>
            {leftIcon && React.cloneElement(
                leftIcon as React.ReactElement,
                { className: "left-icon" }
            )}
            <input onChange={handleChange} {...shared} />
            {rightIcon && React.cloneElement(
                rightIcon as React.ReactElement,
                { className: "right-icon" }
            )}
        </div>
    );
};

export default Input;
