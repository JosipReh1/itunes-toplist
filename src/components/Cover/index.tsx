import React from 'react';
import {Itunes} from "../../icons";
import './cover.scss';

const Cover: React.FC = () => {
    return (
        <div className="cover">
            <div className="logo-container">
                <Itunes />
            </div>
            <span>iTunes top list</span>
        </div>
    )
};

export default Cover;
