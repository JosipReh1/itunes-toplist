import React from 'react';
import './formGroup.scss';

interface Props {
    label?: string;
}
const FormGroup: React.FC<Props> = ({children, label = ''}) => {
    return (
      <div className='form-group'>
          {label && <label>{label}</label>}
          {children}
      </div>
    );
};

export default FormGroup;
