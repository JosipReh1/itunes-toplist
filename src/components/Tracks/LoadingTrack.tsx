import React from 'react';
import {Loading} from "../../icons";

const LoadingTrack = () => (
    <div className='loading-animation'>
        <Loading/>
    </div>
);

export default LoadingTrack;
