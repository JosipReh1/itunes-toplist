import React from 'react';
import TrackCard from "./TrackCard";
import {Loading} from "../../icons";
import {TrackDto} from "../../dto/track.dto";
import './tracks.scss';
import LoadingError from "../LoadingError";

interface Props {
    data: TrackDto[],
    isLoading: boolean;
    hasError: boolean;
    onTrackClick: (track: TrackDto) => void;
}
const TrackList: React.FC<Props> = ({ data = [], isLoading, hasError, onTrackClick }) => {

    const renderTrack = () => <div className="grid-row">
        {data.map(track => <TrackCard key={track.id} track={track} onTrackClick={onTrackClick} />)}
    </div>;

    return (
        <div className='tracks container'>
            { isLoading ? <Loading /> : renderTrack()}
            { hasError && <LoadingError/>}
        </div>
    )
};

export default TrackList;
