import React from 'react';
import {TrackDto} from "../../dto/track.dto";

interface Props {
    track: TrackDto;
    onTrackClick: (track: TrackDto) => void;
}
const TrackCard: React.FC<Props> = ({track, onTrackClick}) => {
    const handleTrackClick = () => {
        onTrackClick(track);
    };
    return (
            <div className='grid-item track' onClick={handleTrackClick}>
                <div className="track-artwork">
                    <img alt={track.artistName} src={track.artworkUrl100} />
                </div>
                <div className="track-details">
                    {track.artistName} - {track.name}
                </div>
            </div>
    );
};

export default TrackCard;
