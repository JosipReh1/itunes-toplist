import React from 'react';
import './searchResultsCount.scss';

interface Props {
    active: number;
    total: number;
}
const SearchResultsCount: React.FC<Props> = ({active, total}) => {
    return (
        <div className='search-results-count'>Showing {active} of {total} results</div>
    );
};

export default SearchResultsCount;
