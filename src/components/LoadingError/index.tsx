import React from 'react';

import './loadingError.scss';
import {useDispatch, useSelector} from "react-redux";
import {selectToplist} from "../../store/toplist/selectors";
import {fetchTracks} from "../../store/toplist/actions";
import {getSelectedItem} from "../Dropdown/util";
import {Refresh} from "../../icons";

const LoadingError = () => {

    const dispatch = useDispatch();

    const {
        form: {
            countries, trackTypes
        }
    } = useSelector(selectToplist);

    const handleRefetch = () => {
        const country = getSelectedItem(countries, 'value');
        const trackType = getSelectedItem(trackTypes, 'value');
        dispatch(fetchTracks(country, trackType))
    };

    return (
        <div className='loading-error'>
            <span className='error'>Loading error</span>
            <div className='refresh-action'><span onClick={handleRefetch}>Refresh <Refresh/></span></div>
        </div>
    )
};

export default LoadingError;
