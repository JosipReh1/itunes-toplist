import React from 'react';

const Itunes: React.FC = () => {
    return (
        <svg
            x="0"
            y="0"
            width="64"
            height="64"
            viewBox="0 0 64 64"
            style={{fill:'#000000'}}
        >
            <linearGradient id="K5582Y0nxj3c5fKX6blGIa" x1="32" x2="32" y1="6" y2="58" gradientUnits="userSpaceOnUse" spreadMethod="reflect">
                <stop offset="0" stopColor="#1a6dff"/>
                <stop offset="1" stopColor="#c822ff"/>
            </linearGradient>
            <path fill="none" stroke="url(#K5582Y0nxj3c5fKX6blGIa)" strokeMiterlimit="10" strokeWidth="2" d="M32 7A25 25 0 1 0 32 57A25 25 0 1 0 32 7Z"/>
            <linearGradient id="K5582Y0nxj3c5fKX6blGIb" x1="29.5" x2="29.5" y1="17" y2="47" gradientUnits="userSpaceOnUse" spreadMethod="reflect">
                <stop offset="0" stopColor="#6dc7ff"/>
                <stop offset="1" stopColor="#e6abff"/>
            </linearGradient>
            <path fill="url(#K5582Y0nxj3c5fKX6blGIb)" d="M41.673,17.018L25.254,20.02C24.514,20.153,24,20.859,24,21.625v15.271 c0,0.527-0.35,0.988-0.851,1.118l-3.984,0.99l-0.328,0.081c-1.481,0.45-2.6,1.754-2.804,3.358c-0.049,0.374-0.045,0.748,0.011,1.123 C16.354,45.599,18.238,47,20.256,47h0.114c3.205,0,5.63-2.614,5.63-5.885V28.957c0-0.78,0.548-1.447,1.3-1.582l12.508-2.335 c0.62-0.126,1.199,0.348,1.199,0.981L41,33.896c0,0.527-0.35,0.988-0.851,1.118l-3.984,0.99l-0.328,0.081 c-1.481,0.45-2.6,1.754-2.804,3.358c-0.049,0.374-0.045,0.748,0.011,1.123C33.354,42.599,35.238,44,37.256,44h0.114 c3.205,0,5.63-2.614,5.63-5.885v-19.96C43,17.437,42.366,16.893,41.673,17.018z"/>
        </svg>
    )
};

export default Itunes;
