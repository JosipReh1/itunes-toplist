import React from 'react';

const App: React.FC<{}> = ({ children }) => {
    return <React.Fragment>{children}</React.Fragment>;
};

export default App;
