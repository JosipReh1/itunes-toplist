import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchTracks, searchTracks, selectCountry, selectFeed} from "../../store/toplist/actions";
import TrackList from "../../components/Tracks";
import {selectToplist} from "../../store/toplist/selectors";
import Cover from "../../components/Cover";
import Input from "../../components/Input";
import Dropdown from "../../components/Dropdown";
import FormGroup from "../../components/FormGroup";
import {getSelectedItem} from "../../components/Dropdown/util";
import {Search} from "../../icons";
import TrackModal from "../../components/TrackModal";
import {TrackDto} from "../../dto/track.dto";
import SearchResultsCount from "../../components/SearchResultsCount";

const HomeLayout: React.FC<{}>= () => {
    const dispatch = useDispatch();

    const [isModalOpen, setModalOpenState] = useState(false);
    const [selectedTrack, setSelectedTrack] = useState();

    const closeModal = () => {
        setModalOpenState(false);
    };

    const openModal = () => {
        setModalOpenState(true);
    };

    const handleTrackClick = (track: TrackDto) => {
        setSelectedTrack(track);
        openModal();
    };

    const {
        filteredData,
        allData,
        form: {
            countries, trackTypes, searchTerm
        }, inProgress,
        error
    } = useSelector(selectToplist);


    const selectedCountry = getSelectedItem(countries, 'value');
    const selectedFeedType = getSelectedItem(trackTypes, 'value');

    const handleTextChange = ({target: {value}}: {target: any}) => {
        dispatch(searchTracks(value));
    };

    const handleCountryChange = (country: string) => {
        dispatch(selectCountry(country))
    };

    const handleFeedTypeChange = (feedType: string) => {
        dispatch(selectFeed(feedType))
    };

    useEffect(() => {
        dispatch(fetchTracks(selectedCountry, selectedFeedType))
    }, [selectedCountry, selectedFeedType, dispatch]);


    return (
        <div>
            <Cover />
            <div className='container grid-row home-body'>
                <FormGroup label='Country'>
                    <Dropdown data={countries} onChange={handleCountryChange}/>
                </FormGroup>
                <FormGroup label='Feed type'>
                    <Dropdown data={trackTypes} onChange={handleFeedTypeChange}/>
                </FormGroup>
                <FormGroup>
                    <Input
                        rightIcon={<Search />}
                        placeholder={'Song or artist'}
                        onChange={handleTextChange}
                        value={searchTerm}
                    />
                </FormGroup>
            </div>
            <SearchResultsCount active={filteredData.length} total={allData.length}/>
            <TrackModal isOpen={isModalOpen} closeModal={closeModal} selectedTrack={selectedTrack} />
            <TrackList data={filteredData} isLoading={inProgress} hasError={error} onTrackClick={handleTrackClick} />
        </div>
    )
};

export default HomeLayout;
