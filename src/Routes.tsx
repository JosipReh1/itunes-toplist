import React from "react";
import { Switch, Route } from 'react-router';
import HomeLayout from "./layouts/HomeLayout";
import App from "./layouts/App";

export default () => (
    <App>
        <Switch>
            <Route exact path="/" component={HomeLayout} />
        </Switch>
    </App>
);
